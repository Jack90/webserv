#include <iostream>
#include "server/Server.h"
#include <stdlib.h>
#include <getopt.h>
#include <cassert>

static const struct option longOptions[] = {
        {"address", 1, NULL, 'a'},
        {"help", 0, NULL, 'h'},
        {"module-dir", 1, NULL, 'm'},
        {"port", 1, NULL, 'p'},
        {"verbose", 0, NULL, 'v'},
};
int main() {
    struct in_addr localAddress;
    uint16_t port;
    int nextOption;
    programName = "";
    localAddress.s_addr = INADDR_ANY;
    port = 0;
    verbose = 0;
    moduleDir = getRootDirictory();
    assert(moduleDir != NULL);
    serverRun(localAddress, port);

    return 0;
}