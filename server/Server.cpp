//
// Created by jack on 4/18/17.
//

#include <sys/wait.h>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include "Server.h"
#include "stdio.h"
#include "../modules/Processes.h"
#include "../modules/DiskFree.h"
#include "Constants.h"
#include <stdlib.h>
#include <signal.h>
#include <assert.h>
#include <cerrno>


static  char* okResponse =
        "HTTP/1.0 200 OK\n"
        "Content-type:text/html\n"
        "\n";

static char* badRequestResponse =
        "HTTP/1.0 400 Bad request\n"
        "Content-type:text/html\n"
        "\n"
        "<html>\n"
        " <body>\n"
                "<div align=\"center\">"
        "  <h1>Bad Request</h1>"
        "  <p>Can`t resolve your request</p>"
                "</div>"
        " </body>\n"
        "</html>\n";

static char* notFoundResponse =
        "HTTP/1.0 404 Not Found\n"
        "Content-type:text/html\n"
        "\n"
        "<html>\n"
        " <body>\n"
        "  <h1>Not Found</h1>"
        "  <p>The request Url was not found</p>"
        " </body>\n"
        "</html>\n";

static char* androidConnectorPage =
        "HTTP/1.0 200 OK\n"
                "Content-type:text/html\n"
                "\n"
                "<html>\n"
                "<head>\n"
                "<meta http-equiv=\"refresh\" content=\"0; url=ftp://fsm/Storage/Quest/vWorkspace/Connector/Android/Release/8.6.7.1548/\" />"
                "</head>\n"
                " <body>\n"
                "<h1><h1>"
                "<a href=\"ftp://fsm/Storage/Quest/vWorkspace/Connector/Android/Release/8.6.7.1548/\">Android connector</a>"
                " </body>\n"
                "</html>\n";

static char* macConnectorPage =
        "HTTP/1.0 200 OK\n"
                "Content-type:text/html\n"
                "\n"
                "<html>\n"
                "<head>\n"
                "<meta http-equiv=\"refresh\" content=\"0; url=ftp://fsm/Storage/Quest/vWorkspace/Connector/Mac/Release/8.6.6/8.6.6.653/\" />"
                "</head>\n"
                " <body>\n"
                "<h1><h1>"
                "<a href=\"ftp://fsm/Storage/Quest/vWorkspace/Connector/Mac/Release/8.6.6/8.6.6.653/\">Mac connector</a>"
                " </body>\n"
                "</html>\n";

static char* iosConnectorPage =
        "HTTP/1.0 200 OK\n"
                "Content-type:text/html\n"
                "\n"
                "<html>\n"
                "<head>\n"
                "<meta http-equiv=\"refresh\" content=\"0; url=ftp://fsm/Storage/Quest/vWorkspace/Connector/iPad/Release/Build%208.6.7/8.6.7.1499%20(May%2030)/\" />"
                "</head>\n"
                " <body>\n"
                "<h1><h1>"
                "<a href=\"ftp://fsm/Storage/Quest/vWorkspace/Connector/iPad/Release/Build%208.6.7/8.6.7.1499%20(May%2030)/\">iOS connector</a>"
                " </body>\n"
                "</html>\n";

static char* linuxConnectorPage =
        "HTTP/1.0 200 OK\n"
                "Content-type:text/html\n"
                "\n"
                "<html>\n"
                "<head>\n"
                "<meta http-equiv=\"refresh\" content=\"0; url=ftp://fsm/Storage/Quest/vWorkspace/Connector/Linux/Release/8.6.5.122/\" />"
                "</head>\n"
                " <body>\n"
                "<h1><h1>"
                "<a href=\"ftp://fsm/Storage/Quest/vWorkspace/Connector/Linux/Release/8.6.5.122/\">Linux connector</a>"
                " </body>\n"
                "</html>\n";

static const char *procStr = "Processes";

class fin;

static void cleanUpChildProcess (int signalNumber) {
    int (status);
    wait (&status);
}


void generateIndexPage(int connectionFd) {
    char buf[2024];
    FILE *file;
    size_t nread;

    file = fopen("ff.txt", "r");
    if (file) {
        while ((nread = fread(buf, 1, sizeof buf, file)) > 0)
            fwrite(buf, 1, nread, stdout);
        if (ferror(file)) {

        }
        fclose(file);
    }
    write(connectionFd, okResponse, strlen(okResponse));
    write(connectionFd, buf, strlen(buf));

//    write(connectionFd, ANDROID_CONNECTOR_LINK, strlen(buf));

}

static void handleGet(int connectionFd, const char* page) {
    struct serverModule* module = NULL;

    if(*page == '/' && strchr(page +1 , '/') == NULL) {
        char moduleFileName[64];
        snprintf(moduleFileName, sizeof(moduleFileName), page + 1);

        if (strstr(moduleFileName, "Android") != NULL) {
            write(connectionFd, okResponse, strlen(okResponse));
            write(connectionFd, androidConnectorPage, strlen(androidConnectorPage));
        } else if (strstr(moduleFileName, "Mac") != NULL) {
            write(connectionFd, okResponse, strlen(okResponse));
            write(connectionFd, macConnectorPage, strlen(macConnectorPage));
        } else if (strstr(moduleFileName, "iOS") != NULL) {
            write(connectionFd, okResponse, strlen(okResponse));
            write(connectionFd, iosConnectorPage, strlen(iosConnectorPage));
        } else if (strstr(moduleFileName, "Linux") != NULL) {
            write(connectionFd, okResponse, strlen(okResponse));
            write(connectionFd, linuxConnectorPage, strlen(linuxConnectorPage));
        } else if (strlen(moduleFileName) == 0)
            generateIndexPage(connectionFd);
        else
            write(connectionFd, notFoundResponse, strlen(notFoundResponse));


    }
}

static void handleConnection(int connectionFd) {
    char buffer[256];
    ssize_t bytesRead;
    bytesRead = read(connectionFd, buffer, sizeof(buffer) - 1);
    if (bytesRead > 0) {
        char method [sizeof(buffer)];
        char url [sizeof(buffer)];
        char protocol [sizeof(buffer)];
        buffer[bytesRead] = '\0';
        sscanf(buffer, "%s %s %s", method, url, protocol);
        while (strstr(buffer, "\r\n\r\n") == NULL)
            bytesRead = read(connectionFd, buffer, sizeof(buffer));

        if (bytesRead == -1) {
            close(connectionFd);
            return;
        }

        if (strcmp(protocol, "HTTP/1.0") && strcmp(protocol, "HTTP/1.1"))
            write(connectionFd, badRequestResponse, sizeof(badRequestResponse));
        else if (strcmp(method, "GET")) {
            char response[1024];
            snprintf(response, sizeof(response), badRequestResponse, method);
            write(connectionFd, response, strlen(response));
        } else
            handleGet(connectionFd, url);
    } else if(bytesRead == 0) {
            ;
    } else
        systemError("read");
}

void serverRun(struct in_addr localAdress, uint16_t port){
    struct sockaddr_in socketAdress;
    int rval;

    struct sigaction sigchldAction;
    int serverSocket;
    memset(&sigchldAction, 0, sizeof(sigchldAction));
    sigchldAction.sa_handler = &cleanUpChildProcess;
    sigaction(SIGCHLD, &sigchldAction, NULL);
    serverSocket = socket(PF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
        systemError("socket");
    memset(&socketAdress, 0, sizeof(socketAdress));
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = port;
    socketAdress.sin_addr = localAdress;
    rval = bind(serverSocket, (const sockaddr *) &socketAdress, sizeof(socketAdress));

    if (rval != 0)
        systemError("bind");

    rval = listen(serverSocket, 10);
    if (rval != 0)
        systemError("listen");

//    if (verbose) {
        socklen_t adressLength;
        adressLength = sizeof(socketAdress);
        rval = getsockname(serverSocket, (sockaddr *) &socketAdress, &adressLength);
        assert( rval == 0 );
        printf("server listening on %s:%d\n", inet_ntoa(socketAdress.sin_addr), ntohs(socketAdress.sin_port));
//    }

    while (1) {
        struct sockaddr_in remoteAdress;
        socklen_t adressLength;
        int connection;
        pid_t childPid;
        adressLength = sizeof(remoteAdress);
        connection = accept(serverSocket, (sockaddr *) &remoteAdress, &adressLength);

        if (connection == -1) {
            if (errno == EINTR)
                continue;
            else
                systemError("accept");
        }
        if (verbose) {
            socklen_t adressLength;
            adressLength = sizeof(socketAdress);
            rval = getpeername(connection, (sockaddr *) &socketAdress, &adressLength);
            assert(rval == 0);
            printf("Connection accepted from %s\n", inet_ntoa(socketAdress.sin_addr));
        }

        childPid = fork();
        if (childPid == 0) {
//            close(STDIN_FILENO);
//            close(STDOUT_FILENO);
            close(serverSocket);
            handleConnection(connection);
            close(connection);
            exit(0);
        } else if (childPid > 0) {
            close(connection);
        } else
            systemError("forkk");

    }
}

void openHtml(){

}