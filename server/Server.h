//
// Created by jack on 4/10/17.
//

#ifndef WEBSERVEREASY_SERVER_H
#define WEBSERVEREASY_SERVER_H

#include <netinet/in.h>
#include <sys/types.h>

extern const char *programName;
extern int verbose;

extern void *xmalloc(size_t size);

extern void *xrealloc(void *ptr, size_t size);

extern char *xstrdup(const char *s);

extern void systemError(const char *operation);

extern void error(const char* cause, const char* message);

extern char* getRootDirictory();

struct serverModule {
    void* handle;
    const char* moduleName;
    void (* generateFunction) (int);
};

extern char* moduleDir;

extern struct serverModule* moduleOpen (const char* modulePath);

extern void moduleClose (struct serverModule* module);

extern void serverRun(struct in_addr local_address, uint16_t port);

void moduleGenerate(int fd);
#endif //WEBSERVEREASY_SERVER_H
