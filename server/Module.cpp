//
// Created by jack on 4/11/17.
//

#include <cstring>
#include <cstdio>
#include <dlfcn.h>
#include <cstdlib>
#include "Server.h"

char *moduleDir;

struct serverModule *moduleOpen(const char *moduleName) {
    char *modulePath;
    void *handle;
    void (*moduleGenerate)(int);
    struct serverModule *module;
    modulePath = (char *) xmalloc(strlen(moduleDir) + strlen(moduleName) + 2);
    sprintf(modulePath, "%s/%s", moduleDir, moduleName);
    handle = dlopen(modulePath, RTLD_NOW);
    free(modulePath);
    if (handle == NULL)
        return NULL;

//    moduleGenerate = (void (*)(int)) dlsym(handle, "moduleGenerate");
    moduleGenerate = (void (*)(int)) dlsym(handle, "moduleGenerate");

    if (moduleGenerate == NULL) {
        dlclose(handle);
        return NULL;
    }
    module = (struct serverModule *) xmalloc(sizeof(struct serverModule));

    module->handle = handle;
    module->moduleName = xstrdup(moduleName);
    module->generateFunction = moduleGenerate;
    return module;
}

void moduleClose(struct serverModule* module)
{
    dlclose(module->handle);
    free((char *) module->moduleName);
    free(module);
}