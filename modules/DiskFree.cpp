//
// Created by jack on 5/15/17.
//


#include <fcntl.h>
#include <unistd.h>
#include "../server/Server.h"

static char* pageStart =
        "<html>\n"
        " <body>\n"
                "  <pre>\n";

static char* pageEnd =
        "  </pre>\n"
                " </body>\n"
                "</html>\n";


void diskModuleGenerate(int fd) {
    pid_t childPid;
    int rval;
    write(fd, pageStart, sizeof(pageStart));
    dup2(fd, STDERR_FILENO);
    dup2(fd, STDOUT_FILENO);
    childPid = fork();
    if (childPid == 0) {
        char* argv[] = {"/bin/df", "-h", NULL};
//        if ( dup2(fd, STDERR_FILENO) == -1 && dup2(fd, STDOUT_FILENO) == -1)
//            systemError("dup2");
        execv(argv[0], argv);
        systemError("execv");
    } else if (childPid > 0) {
//        if (waitpid(childPid, NULL, 0) == -1);
//        systemError("waitPid");
    } else {
        write(fd, pageEnd, sizeof(pageEnd));
    }
}