//
// Created by jack on 5/10/17.
//

#include <fcntl.h>
#include <unistd.h>
#include <memory.h>
#include <sys/sendfile.h>

static char* pageStart =
        "<html>\n"
                " <body>\n"
                "  <pre>\n";

static char* pageEnd =
        "  </pre>\n"
                " </body>\n"
                "</html>\n";

static char* errorPage =
        "<html>\n"
                " <body>\n"
                "  <p>Error: could not open /etc/issue. </p>\n"
                " </body>\n"
                "</html>\n";

static char* errorMessage = "Error reading /etc/issue.";

void moduleGenerate(int fd) {
    int inputFd;
    struct stat fileInfo;
    int rval;
    inputFd = open("/etc/issue", O_RDONLY);
    if (inputFd == -1)
        write(fd, errorPage, strlen(errorPage));
    else {
        int rval;
        off_t  offset = 0;
        write(fd, pageStart, strlen(pageStart));
        rval = sendfile(fd, inputFd, &offset, fileInfo.st_size);
        if (rval == -1)
            write(fd, errorMessage, sizeof(errorMessage));

        write(fd, pageEnd, sizeof(pageEnd));
    }

    close(inputFd);
}