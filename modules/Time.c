//
// Created by jack on 5/10/17.
//

#include <sys/time.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

static char* pageTemplate =
        "<html>\n"
                " <head>\n"
                "  <meta http-equiv=\"refresh\" content=\"5\">\n"
                " </head>\n"
                " <body>\n"
                "  <p> The current time is %s </p>"
                " </body>\n"
                "</html>\n";

void moduleGenerate(int fd) {

    struct timeval tv;
    struct tm* ptm;
    char timeString[40];
    FILE* fp;
    gettimeofday(&tv, NULL);
    ptm = localtime(&tv.tv_sec);
    strftime(timeString, sizeof(timeString), "%H:%M:%S", ptm);
    fp = fdopen(fd, "w");
    assert(fp != NULL);
    fprintf(fp, pageTemplate, timeString);
    fflush(fp);
}
