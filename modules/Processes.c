//
// Created by jack on 5/10/17.
//

#include <stdio.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <pwd.h>
#include <grp.h>
#include <fcntl.h>
#include <unistd.h>
#include <memory.h>
#include <stdlib.h>
#include <dirent.h>
#include "../server/Server.h"
#include "../server/Common.cpp"

static int getUidGid(pid_t pid, uid_t* uid, gid_t* gid) {
    char dirName[64];
    struct stat dirInfo;
    int rval;
    snprintf(dirName, sizeof(dirName), "/proc/%d", (int) pid);
    rval = stat(dirName, &dirInfo);
    if (rval != 0)
        return 1;
    assert(S_ISDIR(dirInfo.st_mode));
    *uid = dirInfo.st_uid;
    *gid = dirInfo.st_gid;
    return 0;
}

static char* getUserName(uid_t uid) {
    struct passwd* entry;
    entry = getpwuid(uid);
//    if (entry == NULL)
//        systemError("getpwuid");
    return xstrdup(entry->pw_name);
}

static char* getGroupName(gid_t gid) {
    struct group* entry;
    entry = getgrgid(gid);
//    if (entry == NULL)
//        systemError("getgrgid");
    return xstrdup(entry->gr_name);
}

static char* getProgramName(pid_t pid){
    char fileName[64];
    char statusInfo[256];
    int fd;
    int rval;
    char* openParen;
    char* closeParen;
    char* result;
    snprintf(fileName, sizeof(fileName), "/proc/%d/stat", (int) pid);
    fd = open(fileName, O_RDONLY);
    if (fd == -1)
        return NULL;
    rval = read(fd, statusInfo, sizeof(statusInfo) - 1);
    close(fd);
    if (rval <= 0)
        return NULL;
    statusInfo[rval] ='\0';
    openParen = strchr(statusInfo, '(');
    closeParen = strchr(statusInfo, ')');
    if (openParen == NULL || closeParen == NULL || closeParen < openParen)
        return NULL;
    result = (char*) xmalloc(closeParen - openParen);
    strncpy(result, openParen - 1, closeParen - openParen - 1);
    result[closeParen - openParen - 1] = '\0';
    return result;
}

static int getRss(pid_t pid) {
    char fileName[64];
    int fd;
    char memInfo[128];
    int rval;
    int rss;
    snprintf(fileName, sizeof(fileName), "/proc/%d/statm", (int) pid);
    fd = open(fileName, O_RDONLY);
    if (fd == -1)
        return -1;
    rval = read(fd, memInfo, sizeof(memInfo) - 1);
    close(fd);
    if (rval <= 0)
        return -1;
    memInfo[rval] ='\0';
    rval = sscanf(memInfo, "%*d %d", &rss);
    if (rval != 1)
        return -1;
    return rss*getpagesize() / 1024;
}

static char* formatProcessInfo(pid_t pid) {
    int rval;
    uid_t  uid;
    gid_t  gid;
    char* userName;
    char* groupName;
    int rss;
    char* programName;
    size_t resultLength;
    char* result;
    rval = getUidGid(pid, &uid, &gid);
    if (rval != 0)
        return  NULL;
    rss = getRss(pid);
    if (rss == -1)
        return NULL;
    programName = getProgramName(pid);
    if (programName == NULL)
        return NULL;
    userName = getUserName(uid);
    groupName = getGroupName(gid);

    resultLength = strlen(programName) + strlen(userName) + strlen(groupName) + 128;
    result = (char*) xmalloc(resultLength);
    snprintf(result, resultLength,
             "<tr><td align=\"right\">%d</td><td><tt>%s</tt></td><td>%s</td><td>%s</td><td align=\"right\">%d</td></tr>\n",
             (int) pid, programName, userName, groupName, rss);
    free(programName);
    free(userName);
    free(groupName);
    return result;
}

static char* pageStart =
        "<html>\n"
                " <body>\n"
                "  <table cellpadding=\"4\" cellspacing=\"0\" border=\"1\">\n>"
                "   <thead>\n"
                "    <tr>\n"
                "     <th>PID</th>\n"
                "     <th>Program</th>\n"
                "     <th>User</th>\n"
                "     <th>Group</th>\n"
                "     <th>RSS&nbsp; (KB)</th>\n"
                "     </tr>\n"
                "    </thead>\n"
                "    </tbody>\n";

static char* pageEnd =
        "   </tbody>\n"
                "  </table>\n"
                " </body>\n"
                "</html>\n";

void moduleGenerate(int fd) {
    size_t  i;
    DIR* procListing;
    size_t vecLength = 0;
    size_t vecSize = 16;
    struct iovec* vec = xmalloc(vecSize * sizeof(struct iovec));
    vec[vecLength].iov_base = pageStart;
    vec[vecLength].iov_len = strlen(pageStart);
    ++vecLength;
    procListing = opendir("/proc");
//    if (procListing == NULL)
//        systemError("openDir");

    while (1) {
        struct dirent* procEntry;
        const char* name;
        pid_t pid;
        char* processInfo;

        procEntry = readdir(procListing);
        if (procEntry == NULL)
            break;
        name = procEntry->d_name;
        if (strspn(name, "0123456789") != strlen(name))
            continue;
        pid = (pid_t) atoi(name);
        processInfo = formatProcessInfo(pid);
        if (processInfo == NULL)
            processInfo = "<tr><td colspan=\"5\">ERROR</td></tr>";
        if (vecLength == vecSize - 1) {
            vecSize *= 2;
            vec = xrealloc(vec, vecSize * sizeof(struct iovec));
        }
        vec[vecLength].iov_base = processInfo;
        vec[vecLength].iov_len = strlen(processInfo);
        ++vecLength;

        writev(fd, vec, vecLength);

        for(int i = 0; i < vecLength - 1; ++i)
            free(vec[i].iov_base);
        free(vec);
    }
}